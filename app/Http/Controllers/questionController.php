<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class questionController extends Controller
{
    public function create() {
        return view('createQuestion');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);
        $query = DB::table('question')
            ->insert([
                'title' => $request['title'],
                'content' => $request['content']
            ]);
        return redirect('/question')->with('success', 'New question successfully created.');
    }

    public function index() {
        $posts = DB::table('question')->get();

        return view('questiontable', compact('posts'));
    }

    public function show($id) {
        $posts = DB::table('question')->where('id', $id)->first();

        return view('detailQuestion', compact('posts'));
    }

    public function edit($id) {
        $posts = DB::table('question')->where('id', $id)->first();

        return view('editQuestion', compact('posts'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        $query = DB::table('question')->where('id', $id)
                ->update([
                    'title' => $request['title'],
                    'content' => $request['content']
                ]);
        return redirect('/question')->with('success', 'Question successfully edited.');
    } 

    public function destroy($id) {
        $query = DB::table('question')->where('id', $id)->delete();
        return redirect('/question')->with('success', 'Question successfully deleted.');
    }
}
