@extends('template.master')

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Create New Question</h3>
            </div>
            <form role="form" action="/question" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                    <input type="Text" class="form-control" id="title" placeholder="Enter Title for question" name="title" value="{{ old('title', '') }}">
                        @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <label for="content">Content</label>
                        <input type="text" class="form-control" id="content" placeholder="Tell me your question" name="content" value="{{ old('content', '')}}">
                        @error('content')
                            <p class="text-danger"> {{ $message }} </p>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                <button type="submit" class="btn btn-dark">Create</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection