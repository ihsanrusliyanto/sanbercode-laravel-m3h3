@extends('template.master')

@section('content')
    <h4> {{ $posts->title }} </h4>
    <p> {{ $posts->content }} </p>
@endsection