@extends('template.master')

@section('content')
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Bordered Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('success'))
                    <p class="text-success"> {{ session('success') }} </p>
                  @endif
                <a href="/question/create" class="btn btn-warning mb-2 float-right">Ask Something</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th style='width: 20vw'>Title</th>
                      <th>Content</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($posts as $key => $post)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $post->title }} </td>
                            <td> {{ $post->content }} </td>
                            <td style="display: flex">
                                <a href="/question/{{ $post->id }}" class="btn btn-info btn-sm mr-1">Detail</a>
                                <a href="/question/{{$post->id }}/edit" class="btn btn-warning btn-sm mr-1">Edit</a>
                                <form action="/question/{{ $post->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center"> No question yet . </td>
                            </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection