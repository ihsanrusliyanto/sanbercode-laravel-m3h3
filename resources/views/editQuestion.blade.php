@extends('template.master')

@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Edit Question</h3>
            </div>
            <form role="form" action="/question/{{ $posts->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                    <input type="Text" class="form-control" id="title" placeholder="Enter Title for question" name="title" value="{{ old('title', $posts->title) }}">
                        @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <label for="content">Content</label>
                        <input type="text" class="form-control" id="content" placeholder="Tell me your question" name="content" value="{{ old('content', $posts->content)}}">
                        @error('content')
                            <p class="text-danger"> {{ $message }} </p>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                <button type="submit" class="btn btn-dark">Edit</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection